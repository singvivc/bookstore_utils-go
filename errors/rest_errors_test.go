package errors

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestInternalServerError(t *testing.T) {
	err := InternalServerError("Internal server error message", errors.New("database error"))
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, "Internal server error message", err.Message)
	assert.EqualValues(t, "internal_server_error", err.Error)

	assert.NotNil(t, err.Causes)
	assert.EqualValues(t, 1, len(err.Causes))
	assert.EqualValues(t, "database error", err.Causes[0])
}

func TestBadRequestError(t *testing.T) {
	err := BadRequestError("Bad request error message")
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusBadRequest, err.Status)
	assert.EqualValues(t, "Bad request error message", err.Message)
}

func TestMysqlError(t *testing.T) {
	err := MysqlError("Database error")

	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusNotAcceptable, err.Status)
	assert.EqualValues(t, "Database error", err.Message)
}

func TestNotFoundError(t *testing.T) {
	err := NotFoundError("not_found")

	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusNotFound, err.Status)
	assert.EqualValues(t, "not_found", err.Message)
}
