package errors

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type RestError interface {
	Message() string
	Status() int
	Error() string
	Causes() []interface{}
}

type restError struct {
	message string        `json:"message"`
	status  int           `json:"status"`
	error   string        `json:"error"`
	causes  []interface{} `json:"causes"`
}

func (r restError) Message() string {
	return r.message
}

func (r restError) Status() int {
	return r.status
}

func (r restError) Causes() []interface{} {
	return r.causes
}

func (r restError) Error() string {
	return fmt.Sprintf("message: %s - status %d - error %s - causes [ %v ]", r.message, r.status,
		r.error, r.causes)
}

func BadRequestError(message string) RestError {
	return restError{message: message, status: http.StatusBadRequest, error: "bad_request"}
}

func NotFoundError(message string) RestError {
	return restError{message: message, status: http.StatusNotFound, error: "not_found"}
}

func UnauthorizedError(message string) RestError {
	return restError{message: message, status: http.StatusUnauthorized, error: "unauthorized_error"}
}

func InternalServerError(message string, errs ...error) RestError {
	errResult := &restError{message: message, status: http.StatusInternalServerError, error: "internal_server_error"}
	if errs != nil && len(errs) > 0 {
		for _, err := range errs {
			errResult.causes = append(errResult.causes, err.Error())
		}
	}
	return errResult
}

func NewRestError(message string, status int, error string, causes []interface{}) RestError {
	return restError{message: message, status: status, error: error, causes: causes}
}

func NewRestErrorFromBytes(bytes []byte) (RestError, error) {
	var apiError restError
	if err := json.Unmarshal(bytes, &apiError); err != nil {
		return nil, errors.New("invalid json")
	}
	return apiError, nil
}

func MysqlError(message string) RestError {
	return &restError{message: message, status: http.StatusNotAcceptable, error: "mysql_error"}
}
