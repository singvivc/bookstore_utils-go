package logger

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"strings"
)

const (
	envLogLevel = "LOG_LEVEL"
	envLogPath  = "LOG_PATH"
)

var log logger

type loggerInterface interface {
	Print(v ...interface{})
	Printf(format string, v ...interface{})
}

type logger struct {
	log *zap.Logger
}

func init() {
	configuration := zap.Config{OutputPaths: []string{getLogPath()}, Level: zap.NewAtomicLevelAt(getLogLevel()),
		Encoding: "json", EncoderConfig: zapcore.EncoderConfig{
			LevelKey: "level", TimeKey: "time", MessageKey: "message",
			EncodeTime: zapcore.ISO8601TimeEncoder, EncodeLevel: zapcore.LowercaseLevelEncoder,
			EncodeCaller: zapcore.ShortCallerEncoder,
		}}
	var err error
	if log.log, err = configuration.Build(); err != nil {
		panic(err)
	}
}

func getLogLevel() zapcore.Level {
	switch os.Getenv(envLogLevel) {
	case "info":
		return zap.InfoLevel
	case "error":
		return zap.ErrorLevel
	case "debug":
		return zap.DebugLevel
	default:
		return zap.InfoLevel
	}
}

func GetLogger() loggerInterface {
	return log
}

func getLogPath() string {
	outputPath := strings.TrimSpace(os.Getenv(envLogPath))
	if outputPath == "" {
		return "stdout"
	}
	return outputPath
}

func (log logger) Print(v ...interface{}) {
	Info(fmt.Sprintf("%v", v))
}

func (log logger) Printf(format string, v ...interface{}) {
	if len(v) == 0 {
		Info(format)
	} else {
		Info(fmt.Sprintf(format, v...))
	}
}

func Info(message string, tags ...zap.Field) {
	log.log.Info(message, tags...)
	log.log.Sync()
}

func Error(message string, err error, tags ...zap.Field) {
	tags = append(tags, zap.NamedError("error", err))
	log.log.Error(message, tags...)
	log.log.Sync()
}

func Warn(message string, tags ...zap.Field) {
	log.log.Warn(message, tags...)
	log.log.Sync()
}
