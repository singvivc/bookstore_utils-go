package date

import "time"

const (
	dateLayout     string = "2006-01-02T15:04:05Z"
	databaseLayout string = "2006-01-02 15:04:05"
)

// CurrentTime returns the current date time
func CurrentTime() time.Time {
	return time.Now().UTC()
}

// GetCurrentTime returns the current time in the string format
func GetCurrentTime() string {
	return CurrentTime().Format(dateLayout)
}

// GetDBFormatTime returns the current time that supports the database format
func GetDBFormatTime() string {
	return CurrentTime().Format(databaseLayout)
}
